class CreateLibros < ActiveRecord::Migration
  def change
    create_table :libros do |t|
      t.string :Titulo
      t.string :string
      t.string :Idioma
      t.string :string
      t.string :Resumen
      t.string :text

      t.timestamps
    end
  end
end
